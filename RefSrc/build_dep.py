#!/usr/bin/env python
from __future__ import print_function

from os       import  listdir, getcwd
from os.path  import  isfile, isdir, join, dirname
from sys      import  argv, stderr

src=dirname(argv[0])
#print("src:",src,argv)
FILE_EXT = [ "f", "F", "F90", "f90" ]

DEF_MOD={}
USE_MOD={}
MOD_DEF={}

#OBJS=[ ]
for fi in listdir( src ) :
    f = join(src,fi)
    if not isfile(f) : continue
    if f.rsplit(".",1)[-1] in FILE_EXT :
        lines=open(f).readlines()
        use  = []
        mods = []
        for line in lines :
            line=line.split()
            if line :
                if line[0].lower() == "module" :
                    if len(line) != 2 : continue
                    mod = line[1].lower()
                    MOD_DEF[mod] = fi
                    mods.append( mod )
                elif line[0].lower() == "use" :
                    mod = line[1].split(",")[0].lower()
                    if mod not in use : use.append( mod )
        USE_MOD[fi] = use
        DEF_MOD[fi] = mods

SPE_MOD=[ "fdf", "mpi_siesta", "siestaxc", "flib_wxml", "parse", "flib_wcml", "matrixswitch" ]
AUT_MOD= [ "omp_lib", "netcdf", "f90_unix", "iso_c_binding", "mpi", "f90_unix_proc", "f90_unix_io" ]
IGN_MOD=[ "flook", "dictionary", "variable", "netcdf_ncdf", "elpa", "f_ppexsi_interface", "extrae_module", "extrae_eventllist" ]
for fi in listdir(src) :
    f = join(src,fi)
    if not isfile(f) : continue
    if fi.split(".")[-1] in FILE_EXT :
        files = [ ]
        spe_mod = [ ]
        for mod in USE_MOD[fi] :
            if mod in MOD_DEF :
                fn = MOD_DEF[mod]
                if fn not in files and fn != fi : files.append( fn )
            elif mod in SPE_MOD :
                if mod not in spe_mod :
                    spe_mod.append( mod )
            elif mod in AUT_MOD : pass
            elif mod in IGN_MOD : pass
            #else : print( fi, "MISSING MODULE", mod, file=stderr )
        msg = "%-22s : %s"%(fi.rsplit(".",1)[0]+".o",fi)
        for fn in files :
            msg += " %s"%(fn.rsplit(".",1)[0]+".o")
        for mod in spe_mod :
#            if   mod == "fdf" : msg += " fdf/fdf.o" if isdir( 'fdf' ) else " ../fdf/fdf.o"
#            elif mod == "parse" : msg += " fdf/parse.o" if isdir( 'fdf' ) else " ../fdf/parse.o"
#            elif mod == "mpi_siesta" : msg += " MPI/mpi_siesta.o" if isdir( 'MPI' ) else " ../MPI/mpi_siesta.o"
#            elif mod == "siestaxc" : msg += " SiestaXC/siestaxc.o" if isdir( 'SiestaXC' ) else " ../SiestaXC/siestaxc.o"
#            elif mod == "flib_wcml" : msg += " wxml/flib_wcml.o" if isdir( 'wxml' ) else " ../wxml/flib_wcml.o"
#            elif mod == "flib_wxml" : msg += " wxml/flib_wxml.o" if isdir( 'wxml' ) else " ../wxml/flib_wxml.o"
            if   mod in [ "fdf", "parse" ] : msg += " fdf/libfdf.a" if isdir( 'fdf' ) else " ../fdf/libfdf.a"
            elif mod == "mpi_siesta" : msg += " MPI/libmpi_f90.a" if isdir( 'MPI' ) else " ../MPI/libmpi_f90.a"
            elif mod == "siestaxc" : msg += " SiestaXC/libsiestaxc.a" if isdir( 'SiestaXC' ) else " ../SiestaXC/libsiestaxc.a"
            elif mod in [ "flib_wcml","flib_wxml" ] : msg += " wxml/libwxml.a" if isdir( 'wxml' ) else " ../wxml/libwxml.a"
            elif mod == "matrixswitch" : msg += " MatrixSwitch/MatrixSwitch.a" if isdir( 'MatrixSwitch' ) else " ../MatrixSwitch/MatrixSwitch.a"
            else :
                print( "MOD", mod )
                exit(0)
        print( msg )
    elif fi.split(".")[-1] in [ "c" ] :
        msg=fi.rsplit(".",1)[0]+".o: "+fi

print( )
print( "%.o: %.F" )
print( "\t$(FC) -c $(FFLAGS) $(FINCLUDES) $(DEFINES) -o $@ $<" )

print( )
print( "%.o: %.f" )
print( "\t$(FC) -c $(FFLAGS) $(FINCLUDES) $(DEFINES) -o $@ $<" )

print( )
print( "%.o: %.F90" )
print( "\t$(FC) -c $(FFLAGS) $(FINCLUDES) $(DEFINES) -o $@ $<" )

print( )
print( "%.o: %.f90" )
print( "\t$(FC) -c $(FFLAGS) $(FINCLUDES) $(DEFINES) -o $@ $<" )

print( )
print( "%.o: %.c" )
print( "\t$(CC) -c $(CFLAGS) $(DEFINES) $(INCLUDES) -o $@ $<" )

print( )
print( "%.o: %.cpp" )
print( "\t$(CXX) -c $(CXXFLAGS) $(DEFINES) $(INCLUDES) -o $@ $<" )
