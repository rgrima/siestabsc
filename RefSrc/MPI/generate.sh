#!/bin/bash
echo " ===> Generating module files from templates..."
dir=$(dirname $0)

KINDS=$( ./kind_explorer 2> /dev/null )
INT_KINDS=$( ./int_explorer 2> /dev/null )

V_S=V_S.uses
VS=VS.uses
INT=Interfaces.f90

rm -rf ${V_S} ${VS} ${INT} &> /dev/null

for kind in ${KINDS} ; do
    echo "         USE MPI__r${kind}_V      ;  USE MPI__r${kind}_S" >> ${V_S}
    echo "         USE MPI__c${kind}_V      ;  USE MPI__c${kind}_S" >> ${V_S}
    echo "         USE MPI__r${kind}_VS      ;  USE MPI__r${kind}_SV" >> ${VS}
    echo "         USE MPI__c${kind}_VS     ;  USE MPI__c${kind}_SV" >> ${VS}
done

for kind in ${INT_KINDS} ; do
  echo "         USE MPI__i${kind}_V      ;  USE MPI__i${kind}_S" >> ${V_S}
  echo "         USE MPI__i${kind}_VS      ;  USE MPI__i${kind}_SV" >> ${VS}
done

for tag in v s sv vs ; do
    for kind in ${KINDS} ; do
        sed -e "/_type/s//_r${kind}/" -e "/type/s//real(${kind})/" ${dir}/mpi__type_${tag}.f90 >> ${INT}
        sed -e "/_type/s//_c${kind}/" -e "/type/s//complex(${kind})/"  ${dir}/mpi__type_${tag}.f90 >> ${INT}
    done
    for kind in ${INT_KINDS} ; do
        sed -e "/_type/s//_i${kind}/" -e "/type/s//integer(${kind})/" ${dir}/mpi__type_${tag}.f90 >> ${INT}
    done
    sed -e "/_type/s//_logical/" -e "/type/s//logical/" ${dir}/mpi__type_${tag}.f90 >> ${INT}
    sed -e "/_type/s//_character/" -e "/type/s//character(*)/" ${dir}/mpi__type_${tag}.f90  >> ${INT}
done
