From this project you can compile the new Siesta version and the version that we
have used as a reference:
* Run 'make' to compile the new version
* Run 'make SRC=RefSrc DESTDIR=RefObj' to compile the reference source code

Edit arch.make to set the proper compiler flags.

It checks mpif90 to detect if we are using INTEL or GNU compilers.

Set USE_LOCAL_SCALAPACK to compile an scalapack version  or set LAPACK_LIBS,
BLAS_LIB and SCALAPACK_LIB.