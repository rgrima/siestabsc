#!/bin/bash
FDF_File=dna.fdf
EXT_files=$( ls *.fdf *.psf *.inp *.DM extrae.xml 2> /dev/null )

TOPDIR=$( realpath $( dirname $0 )/../.. )
EXE=${TOPDIR}/Obj/siesta
NTHREADS=1

#for n in 1 2 3 4 6 8 10 12; do
for n in 14 16 18 20 22 24 26 28 30 32; do
    NPROCS=$(( n*24 ))
    echo "###### EXECUTING with ${NPROCS} procs"
    WDIR=$( printf "WEAK-%04d" ${NPROCS} )
    [ -d ${WDIR} ] && rm -rf ${WDIR}; mkdir ${WDIR}
    cp ${EXT_files} ${WDIR}
    pushd ${WDIR} &> /dev/null

    # Let's grow the problem in different directions
    factors=()
    while [ "${n}" -gt 1 ]; do
        i=2
        while [ $(( $n -($n/$i)*i )) -ne 0 ]; do i=$(( i+1 )); done
        factors+=( $i )
        n=$(( $n/$i ))
    done
    esc=(1 1 1)
    for i in $( seq ${#factors[@]}); do
        v=${factors[-$i]}
        [[ ${esc[2]} -le ${esc[1]} && ${esc[2]} -le ${esc[0]} ]] && esc[2]=$(( ${esc[2]}*$v )) ||
            { [ ${esc[1]} -le ${esc[0]} ] && esc[1]=$(( ${esc[1]}*$v )) || esc[0]=$(( ${esc[0]}*$v )); }
    done
    line=$( grep -n "%block SuperCell" ${FDF_File} | cut -d":" -f 1 )
    { head -n ${line} ${FDF_File}
    echo "${esc[0]} 0 0"
    echo "0 ${esc[1]} 0"
    echo "0 0 ${esc[2]}"
    line=$( grep -n "%endblock SuperCell" ${FDF_File} | cut -d":" -f 1 )
    tail -n +${line} ${FDF_File}
    } > rgt.fdf; mv rgt.fdf ${FDF_File}

    if [ ! -z "${SLURM_NODELIST}" ]; then
        for line in $( scontrol show hostnames $SLURM_JOB_NODELIST ); do
            echo "$line:48"
        done > hfile
    else
        echo "localhost:${NPROCS}" > hfile
    fi

    SC=./siesta.sh

cat << EOF > ${SC}
#!/bin/bash
ulimit -c unlimited
export OMP_NUM_THREADS=${NTHREADS}
ulimit -s unlimited
${EXE} ${FDF_File}
EOF

    chmod +x ${SC}

    mpirun -np ${NPROCS} --hostfile hfile ${SC}
    
    popd &> /dev/null
done
