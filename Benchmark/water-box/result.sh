#!/bin/bash
TAGS="siesta Setup_H0 rhoofd POISON XC vmat compute_dm  dfscf"
printf "    "
for tag in ${TAGS}; do
    printf "\t%-12s" $tag
done
echo

bname="strong-"
for dir in $( ls | grep ${bname} ); do
    n=${#bname}
    i=$( printf "%d\n" $( echo ${dir} | cut -c $(( n+1 ))- | sed 's/^0*//' ) )
    file="${dir}/TIMES"
    if [ -f ${file} ]; then
        printf "%4d " $i
        for tag in ${TAGS}; do
            msg=$( grep -w $tag ${file} )
            msg=$( echo $msg | cut -d" " -f 5 )
            printf "\t%-12s" $msg
        done
        echo
    else
        file="${dir}/time.json"
        if [ -f ${file} ]; then
            printf "%4d " $i
            for tag in ${TAGS}; do
                msg=$( grep -w $tag ${file} | cut -d":" -f 4 | cut -d"," -f 1 | xargs | sed 's/ /+/g' | bc -l )
                printf "\t%-12s" $msg
            done
            echo
        else
            echo "MISSING time file in PATH=${dir}"
        fi

    fi
done


