#!/usr/bin/env python

from os.path import isfile

def readColors( lines, color, n=0 ) :
    i = n
    while i< len(lines) and lines[i] != "STATES_COLOR" :
        i = i+1
    first = i
    i = i+1
    while lines[i] :
        color += [ lines[i].split()[1] ]
        i = i+1
    return first

def readEventType( lines, Type, n=0 ) :
    i = n
    while i< len(lines) and "60000019" not in lines[i] :
        i = i+1
    first = i
    i = i+2
    while lines[i] :
        Type += [ lines[i].split(" ",1)[1] ]
        i = i+1
    return first



f1="wGNU/siesta.pcf"
f2="wGNURef/siesta.pcf"
if not isfile(f1) : exit( )
if not isfile(f2) : exit( )

LINES1=[ line.strip() for line in open(f1,"r").readlines() ]
color1=[]
fc1 = readColors( LINES1, color1 )
print 'fc1', fc1

LINES2=[ line.strip() for line in open(f2,"r").readlines() ]
color2=[]
fc2 = readColors( LINES2, color2, fc1 )
print 'fc2', fc2
print len(color1), len(color2)

if len(color1) != len(color2) :
    print 'need to extend'
    if len(color1) > len(color2) :
        fc1_ = fc1
        fc2_ = fc2
        L1 = LINES1
        L2 = LINES2
        c1 = color1
        c2 = color2
    else :
        fc1_ = fc2
        fc2_ = fc1
        L1 = LINES2
        L2 = LINES1
        c1 = color2
        c2 = color1
    nc1 = len(c2)
    nc2 = len(c1)
    for i in range(nc2,nc1) :
        L2.insert( fc2_+i+1, L1[i] )
        c2 += L1[i]

print len(color1), len(color2)

Type1 = []
fe1 = readEventType( LINES1, Type1, fc1 )

Type2 = []
fe2 = readEventType( LINES2, Type2, fc2 )

maxc = max(len(color1),len(color2))
maxt = max(len(Type1),len(Type2))

if maxt > maxc :
    dn = maxt-maxc+1
    for i in range(1,dn) :
        v=i*255/dn
        x= '%d    {%d,%d,%d}'%(maxc+i-1, v, v, v )
        LINES1.insert( fc1+maxc+i, x )
        LINES2.insert( fc2+maxc+i, x )
        color1 += [ '{%d,%d,%d}'%( v, v, v ) ]
        color2 += [ '{%d,%d,%d}'%( v, v, v ) ]

for i, t in enumerate(Type2) :
    ind = Type1.index( t )
    LINES2[fc2+i+1] = '%d'%i+'    '+color1[ind]

open(f1,"w").write( "\n".join( LINES1 ) )
open(f2,"w").write( "\n".join( LINES2 ) )
