# FDF input file for A-DNA (11 GC base pairs) Relaxed and symmetrised.
# Based on file by Emilio Artacho 1998
#                  Improved basis. Emilio Artacho 2004

#---- USAGE COMMENTS --
#
# Use this block to change the size of the system for benchmarks
# The base system has around 700 atoms.
# Changing it to, for example (diagonally) 2 2 2 will use 8-times more atoms.
#
%block SuperCell
  1   0   0
  0   1   0
  0   0   1
%endblock SuperCell

# This parameter controls the granularity of the parallel decomposition over
# orbitals (block-cyclic) and also the Scalapack blocksize
#
BlockSize 8

# This parameter controls the fineness of the real-space grid. The higher the finer.
# It is important for performance of the routines called from dhscf, notably rhoofd
# and vmat.

MeshCutoff           140. Ry

#  For benchmarks this might be enough
#
MaxSCFIterations      1

# -- No writing of DM in principle. Might be useful to measure also
#
#WriteDenchar .true.
WriteDM      .false.
#WriteH .true.
#WriteDM.end.of.cycle .true.
#WriteH.end.of.cycle .true.

# Change to 'diagon' to measure diagonalization
# PEXSI has been upgraded recently and is not interfaced yet.
#
SolutionMethod        diagon
##SolutionMethod        pexsi

# The following can stay unchanged
#---------------------------------------------------------
SystemName         DNA
SystemLabel        DNA


# SCF options
#DM.UseSaveDM          yes
#
#
DM.MixingWeight       0.10
DM.Tolerance          1.d-2
DM.NumberPulay        3
#DM.PulayOnFile        true

#------- PEXSI options will be ignored for 'diagon' option
#MPI.Nprocs.SIESTA 640
PEXSI.np-per-pole 4
PEXSI.ordering 1
PEXSI.np-symbfact 16

# PEXSI variables
PEXSI.inertia-count 1
PEXSI.inertia-counts 100
PEXSI.inertia-min-num-shifts 40
PEXSI.inertia-num-electron-tolerance 150
PEXSI.mu-max-iter 30
PEXSI.temperature 300.0
PEXSI.num-poles 40
PEXSI.mu  -0.30 Ry
PEXSI.muMin -2.00 Ry
PEXSI.muMax 0.00 Ry
PEXSI.num-electron-tolerance-upper-bound 1
PEXSI.num-electron-tolerance-lower-bound 0.01
DM.NormalizationTolerance 1.0d-1  # (true_no_electrons/no_electrons) - 1.0 


# Species parameters
NumberOfSpecies    5
%block ChemicalSpeciesLabel
  1   1   H
  2   6   C
  3   7   N
  4   8   O
  5  15   P
%endblock ChemicalSpeciesLabel


# Basis set options

PAO.SplitNorm 0.3
%Block PAO.Basis
P   2
 n=3   0   2
    6. 0.
 n=3   1   2   P
    6. 0.
H    2      0.76857
 n=1   0   2   E    45.37962     4.20457
     6.06811     1.85004
 n=2   1   1   E    40.08175     2.95945
     4.74784
C    3      0.09879
 n=2   0   2   E    39.37755     3.60560
     5.20021     2.85570
 n=2   1   2   E    95.68842     4.31202
     5.68648     2.94880
 n=3   2   1   E    62.49576     0.66804
     3.97291
N    3     -0.00139
 n=2   0   2   E    65.50216     4.29661
     5.64483     3.02914
 n=2   1   2   E    30.54417     5.81284
     6.20000     2.85547
 n=3   2   1   E    59.15335     0.14049
     3.65788
O    3     -0.24021
 n=2   0   2   E    64.35698     2.24090
     4.39710     2.52029
 n=2   1   2   E     7.99069     4.88285
     6.12535     2.30982
 n=3   2   1   E    20.95104     3.07361
     5.07487
%EndBlock PAO.Basis

# Unit cell and atomic positions
LatticeConstant                   < ApGpC_xyz_goodbas.fdf
LatticeVectors                    < ApGpC_xyz_goodbas.fdf
NumberOfAtoms                     < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesFormat           < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesOrigin           < ApGpC_xyz_goodbas.fdf
AtomicCoordinatesAndAtomicSpecies < ApGpC_xyz_goodbas.fdf

XC.Functional         GGA
XC.Authors            PBE

#NeglNonOverlapInt     yes

# Diagon options
ElectronicTemperature  5 meV 

