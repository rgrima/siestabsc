#!/bin/bash
TOPDIR=$( realpath $( dirname $0 )/../.. )
FDF_File=dna-pexsi.fdf

IS_INTEL=true
IS_REFER=false

${IS_REFER} && { SRCDIR=${TOPDIR}/RefSrc; REFDIR=${TOPDIR}/Ref; LOC_SCA=FALSE; } ||
               { SRCDIR=${TOPDIR}/Src;    REFDIR=${TOPDIR}/; LOC_SCA=TRUE; }
${IS_INTEL} && REFDIR=${REFDIR}Obj || REFDIR=${REFDIR}GNU

WDIR=w$( ${IS_INTEL} && echo ork || echo GNU )$( ${IS_REFER} && echo Ref )
EXE=${REFDIR}/siesta

NPROCS=48
NTHREADS=1

[ $# -ge 1 ] && [ $1 -eq $1 ] &> /dev/null && [ $1 -gt 0 ] && NPROCS=$1
[ $# -ge 2 ] && [ $2 -eq $2 ] &> /dev/null && [ $2 -gt 0 ] && NTHREADS=$2
[ $# -ge 3 ] && WDIR=$3

if ! ${IS_INTEL}; then
   module purge
   #module load gcc/7.2.0 openmpi/3.1.1 hdf5/1.10.1-ts netcdf/4.6.1
   module load gcc/7.1.0 openmpi
   export SCALAPACK_HOME=/apps/SCALAPACK/2.0.2/GCC/OPENMPI
   export LAPACK_HOME=/apps/LAPACK/3.8.0/GCC
   export BLAS_HOME=/apps/LAPACK/3.8.0/GCC
fi
pushd ../..
#make -j ${NPROCS} SRC=${SRCDIR} DESTDIR=${REFDIR} USE_LOCAL_SCALAPACK=${LOC_SCA} || exit
make -j ${NPROCS} SRC=${SRCDIR} DESTDIR=${REFDIR} || exit
popd

EXT_files=$( ls *.fdf *.psf *.inp *.DM extrae.xml 2> /dev/null )

if [ -d ${WDIR} ]; then
  Id=1
  DDIR=$( printf "${WDIR}.%02d" ${Id} )
  while [ -d "${DDIR}" ]; do Id=$(( Id + 1 )); DDIR=$( printf "${WDIR}.%02d" ${Id} ); done
  mv ${WDIR} ${DDIR}
fi

mkdir ${WDIR}
cp ${EXT_files} ${WDIR}
cd ${WDIR}

MSG=$( nm -a ${EXE} )
if ${IS_INTEL}; then
    funcs="
        m_siesta_init_mp_siesta_init_
        m_siesta_forces_mp_siesta_forces_
            m_state_init_mp_state_init_
            m_setup_h0_mp_setup_h0_
            m_setup_hamiltonian_mp_setup_hamiltonian_
                m_dhscf_mp_dhscf_
                    m_rhoofd_mp_rhoofd_
                    poison_
                    m_cellxc_mp_cellxc_
                    m_vmat_mp_vmat_
                    m_dfscf_mp_dfscf_
            m_compute_dm_mp_compute_dm_
            m_compute_energies_mp_compute_energies_
            m_scfconvergence_test_mp_scfconvergence_test_
            m_siesta_forcessiesta_forces_mp_compute_forces_
        m_siesta_move_mp_siesta_move_
        "
else
    funcs="
        __m_siesta_init_MOD_siesta_init
        __m_siesta_forces_MOD_siesta_forces
            __m_state_init_MOD_state_init
            __m_setup_h0_MOD_setup_h0
            __m_setup_hamiltonian_MOD_setup_hamiltonian
                __m_dhscf_MOD_dhscf
                    __m_rhoofd_MOD_rhoofd
                    poison_
                    __m_cellxc_MOD_cellxc
                    __m_vmat_MOD_vmat
                    __m_dfscf_MOD_dfscf
            __m_compute_dm_MOD_compute_dm
            __m_compute_energies_MOD_compute_energies
            __m_scfconvergence_test_MOD_scfconvergence_test
            compute_forces.3722
            compute_forces.3995
        __m_siesta_move_MOD_siesta_move
        "
fi
for f in ${funcs}; do
    line=( $( echo "$MSG" | grep -w "${f}$" ) )
    echo "${line[0]}#${line[2]}" >> functions.dat
done

LINES=( $( grep -n -e ChemicalSpeciesLabel -e Chemical_Species_Label ${FDF_File} | cut -d":" -f 1 ) )
line=$(( ${LINES[0]} + 1 ))
while [ "${line}" -lt "${LINES[1]}" ]; do
    field=$( sed "${line}q;d" ${FDF_File} | xargs | cut -d" " -f 3 )
    if [ ! -f ${field}.psf ]; then
      PSF_FILE=$( find ${TOPDIR}/Tests/Pseudos -name ${field}.psf | head -n 1 )
      [ -z "${PSF_FILE}" ] && echo "MISSING ${field}.psf file" || cp ${PSF_FILE} .
    fi
    line=$(( ${line} + 1 ))
done

if [ ! -z "${BSC_MACHINE}" ]; then
#    export EXTRAE_HOME=/apps/BSCTOOLS/extrae/3.5.2/impi_2017_4/
#    export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
#    export EXTRAE_HOME=/apps/BSCTOOLS/extrae/3.5.4-rc1/impi_2017_4/
#    ${IS_INTEL} && export EXTRAE_HOME=/apps/BSCTOOLS/extrae/3.6.1/impi_2017_4 ||
     ${IS_INTEL} && export EXTRAE_HOME=/apps/BSCTOOLS/extrae/devel/git-2781fce6/impi_2017_4 ||
                    export EXTRAE_HOME=/apps/BSCTOOLS/extrae/3.5.4/openmpi_1_10_7
else
    [ -d /opt/extrae-3.6.1 ] && export EXTRAE_HOME=/opt/extrae-3.6.1
    [ -d /opt/extrae-3.5.2 ] && export EXTRAE_HOME=/opt/extrae-3.5.2
    [ -d /opt/extrae-3.5.4 ] && export EXTRAE_HOME=/opt/extrae-3.5.4
fi

SC=./siesta.sh

${IS_INTEL} && HFILE_FORMAT=":" || HFILE_FORMAT=" slots="
#mpirun --version | grep "Intel" &> /dev/null && HFILE_FORMAT=":"
#mpirun --version | grep "Open MPI" &> /dev/null && HFILE_FORMAT=" slots="

if [ ! -z "${SLURM_NODELIST}" ]; then
    for line in $( scontrol show hostnames $SLURM_JOB_NODELIST ); do
        echo "$line${HFILE_FORMAT}48"
    done > hfile
else
    echo "localhost${HFILE_FORMAT}${NPROCS}" > hfile
fi

cat << EOF > ${SC}
#!/bin/bash
ulimit -c unlimited
#export OMP_NUM_THREADS=${NTHREADS}
[ ! -z "${OMP_NUM_THREADS}" ] && unset OMP_NUM_THREADS
#export EXTRAE_ON=1
#export EXTRAE_DISABLE_MPI=1
#export LD_PRELOAD=${EXTRAE_HOME}/lib/libmpitracecf.so
export EXTRAE_CONFIG_FILE=extrae.xml
#ulimit -s unlimited
#valgrind  --max-stackframe=6000000 
${EXE} ${FDF_File}
EOF

chmod +x ${SC}

mpirun -np ${NPROCS} --hostfile hfile ${SC} 2>&1 | tee output
