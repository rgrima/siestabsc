# FDF input file for A-DNA (11 GC base pairs) Relaxed and symmetrised.
# A. Garcia
# Based on file by Emilio Artacho 1998
#                  Improved basis. Emilio Artacho 2004

#---- USAGE COMMENTS AND TUNABLE PARAMETERS ---
#
# Use this block to change the SIZE of the system for benchmarks
# The base system has 720 atoms.
# Changing it to, for example (diagonally) 2 2 2 will use 8-times more atoms.
#
%block SuperCell
  1   0   0
  0   1   0
  0   0   1
%endblock SuperCell

# This parameter controls the fineness of the real-space grid. The higher the finer.
# It is important for performance of the routines called from dhscf, notably rhoofd
# and vmat.
# For this system a large cutoff is not really needed, but for benchmarking purposes
# it can go up to, say, 500 Ry, maybe 1000 Ry

MeshCutoff           140. Ry    

#  For benchmarks this might be enough (see below, and the general notes)
#
MaxSCFIterations      5
scf-must-converge F       # Needed to finish the program gracefully with the above line.


#MD.TypeOfRun         cg
MD.NumCGsteps        1

# -- No writing of DM in principle. Might be useful to measure also
#
WriteDM      .false.
#
#---------------------------------------------------------------
# This parameter controls the granularity of the parallel decomposition
# over orbitals (block-cyclic) and also the Scalapack blocksize
#
BlockSize 8
#---------------------------------------------------------------

#  SOLVER ------------------
#
SolutionMethod        pexsi   # Need to compile SIESTA with -DSIESTA__PEXSI (see Manual)
#
#------- PEXSI options will be ignored for 'diagon' option
#
# With these numbers, full parallelization in PEXSI will be achieved with 640 procs
# Number of procs per pole-team
PEXSI.np-per-pole 16
PEXSI.num-poles 40
#-------------------------
# It might be necessary to reduce the number of nodes working on the non-solver parts
#
#MPI.Nprocs.SIESTA 120             # Set this for non-solver steps
#
#-------------------------------------------- The rest should not need to be changed,
#                                             except this, if we know an approx value:
PEXSI.mu  -0.30 Ry
#
PEXSI.ordering 1
PEXSI.np-symbfact 16

PEXSI.inertia-count 1
PEXSI.inertia-counts 100
PEXSI.inertia-min-num-shifts 40
PEXSI.inertia-num-electron-tolerance 150
PEXSI.mu-max-iter 30
PEXSI.temperature 300.0
PEXSI.muMin -2.00 Ry
PEXSI.muMax 0.00 Ry
PEXSI.num-electron-tolerance-upper-bound 1
PEXSI.num-electron-tolerance-lower-bound 0.01
DM.NormalizationTolerance 1.0d-1  # (true_no_electrons/no_electrons) - 1.0 
#
# ---- 
#SolutionMethod        diagon
#
# This option (no solver step) can be enabled in the code via a patch
#SolutionMethod        dummy
#
# This option will stop the program after the setup of H (avoiding solver and forces).
#
# setup-H-only T
#---------------------------

# Basis set options -------------
# Choose one line
# 
include dna.basis.dzp.fdf             # Default "moderate" basis
# PAO.BasisSize sz                    # Minimal basis
# PAO.BasisSize tzp                   # Larger cardinality
#---------------------------------

# This option will make H more sparse by avoiding counting interactions
# among orbitals mediated only by a KB projector on a third atom.
#NeglNonOverlapInt     yes

#=========================================================
# The following can stay unchanged
#---------------------------------------------------------
SystemName         DNA
SystemLabel        DNA

#UseTreeTimer      T
UseTreeTimer      F
UseParallelTimer  T

# SCF options
#DM.UseSaveDM          yes
#
#
DM.MixingWeight       0.10
DM.Tolerance          1.d-3          # Could be changed to 1.d-4, etc
DM.NumberPulay        3

# Species parameters
NumberOfSpecies    5
%block ChemicalSpeciesLabel
  1   1   H
  2   6   C
  3   7   N
  4   8   O
  5  15   P
%endblock ChemicalSpeciesLabel

# Unit cell and atomic positions
LatticeConstant                   < dna.geometry.fdf
LatticeVectors                    < dna.geometry.fdf
NumberOfAtoms                     < dna.geometry.fdf
AtomicCoordinatesFormat           < dna.geometry.fdf
AtomicCoordinatesOrigin           < dna.geometry.fdf
AtomicCoordinatesAndAtomicSpecies < dna.geometry.fdf

XC.Functional         GGA
XC.Authors            PBE

# Diagon options
ElectronicTemperature  5 meV 

