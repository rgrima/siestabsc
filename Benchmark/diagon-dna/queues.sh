#!/bin/bash

FDF_File=dna-pexsi.fdf
EXT_files=$( ls *.fdf *.psf *.inp *.DM extrae.xml 2> /dev/null )
WdirBase=strong-

TOPDIR=$( realpath ../.. )
EXE=${TOPDIR}/Obj/siesta

for N in $( seq 16 16 224 ); do
#for N in $( seq 8 ); do
    n=$(( 48*N ))
    [ ${N} -lt 48 ] && npp=48 || npp=${N}
    id=$( printf "%05d" ${n} )

    WDIR=${WdirBase}${id}
    [ -d ${WDIR} ] && rm -rf ${WDIR}
    mkdir ${WDIR}
    cp ${EXT_files} ${WDIR}
    pushd ${WDIR} &> /dev/null

    line=$( grep -n "PEXSI.np-per-pole" ${FDF_File} | cut -d":" -f 1 )
    {
        head -n $(( line-1 )) ${FDF_File}
        echo "PEXSI.np-per-pole ${npp}"
        echo "PEXSI.num-poles 48"
        tail -n +$(( line+2 )) ${FDF_File}
    } > rgt.fdf; mv rgt.fdf ${FDF_File}

    mpirun --version | grep "Intel" &> /dev/null && HFILE_FORMAT=":"
    mpirun --version | grep "Open MPI" &> /dev/null && HFILE_FORMAT=" slots="

    Script=queue.sh
    JNAME=${id}.PX
    OUT=out.${id}
    ERR=err.${id}
    QOS=
    [ ${N} -ge 50 ] && QOS="#SBATCH --qos=xlarge"
    [ ${N} -le 16 ] && QOS="#SBATCH --qos=debug"
    cat << EOF > ${Script}
#!/bin/bash
#SBATCH --job-name=${JNAME}
#SBATCH --workdir=.
#SBATCH --output=${OUT}
#SBATCH --error=${ERR}
#SBATCH --ntasks=${n}
#SBATCH --cpus-per-task=1
#SBATCH --time=02:00:00
${QOS}

for line in \$( scontrol show hostnames \${SLURM_JOB_NODELIST} ); do
    echo "\${line}${HFILE_FORMAT}48"
done > hfile

mpirun -np ${n} --hostfile hfile ${EXE} ${FDF_File}

EOF
    sbatch ${Script}
    popd &> /dev/null
done


