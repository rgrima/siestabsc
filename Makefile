TOPDIR=$(shell pwd)
ARCH=$(TOPDIR)/arch.make

ifndef DESTDIR
	DESTDIR=$(TOPDIR)/Obj
endif

ifndef SRC
	SRCpath=$(TOPDIR)/Src
else
        SRCpath=$(realpath $(SRC))
endif

default: siesta

siesta: $(DESTDIR)/Makefile
	$(MAKE) -C $(DESTDIR) ARCH=$(ARCH) SRC=$(SRCpath)

$(DESTDIR)/Makefile:
	@[ ! -d $(DESTDIR) ] && mkdir $(DESTDIR)
	@ln -s $(SRCpath)/Makefile $(DESTDIR)
	$(MAKE) -C $(DESTDIR) ARCH=$(ARCH) SRC=$(SRCpath) MAKEFILES

clean:
	$(MAKE) -C $(DESTDIR) ARCH=$(ARCH) SRC=$(SRCpath) clean

cleanpexsi:
	$(MAKE) -C $(DESTDIR) ARCH=$(ARCH) SRC=$(SRCpath) $@

cleansuperlu:
	$(MAKE) -C $(DESTDIR) ARCH=$(ARCH) SRC=$(SRCpath) $@

