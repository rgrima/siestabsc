GNU=$(shell mpif90 --version | head -n 1 | cut -d" " -f1)
FFLAGS   = -g -O3
CFLAGS   = -g -O2
# -std=c99
CXXFLAGS = -g -O3 -std=c++11

USE_LOCAL_SCALAPACK  ?= FALSE
USE_EXTRAE           ?= FALSE
INSTRUMENT_FUNCTIONS ?= FALSE

ifeq ($(GNU),GNU)
    CC              = mpicc
    CXX             = mpic++

    #CFLAGS         += -fopenmp
    #CFLAGS          += -Wall -Wextra -pedantic -Wshadow -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -D_FORTIFY_SOURCE=2 -Wshift-overflow=2 -Wduplicated-cond 
    #CXXFLAGS        += -Wall -Wextra -pedantic -Wshadow -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -D_FORTIFY_SOURCE=2 -Wshift-overflow=2 -Wduplicated-cond 
    INCLUDES        =

    FC              = mpif90
    #FFLAGS         += -fcheck=all -fbacktrace -ffpe-trap=zero,overflow -finit-real=nan
    #  -fopenmp  -ffpe-trap=underflow
    FINCLUDES       = -I/usr/include

    ifeq ($(USE_LOCAL_SCALAPACK),FALSE)
        ifdef LAPACK_HOME
            LAPACK_LIBS     = -L$(LAPACK_HOME)/lib64 -llapack -Wl,-rpath=$(LAPACK_HOME)/lib64
        else
            LAPACK_LIBS     = -llapack
        endif
        ifdef BLAS_HOME
            BLAS_LIBS       = -L$(BLAS_HOME)/lib64 -lblas -Wl,-rpath=$(BLAS_HOME)/lib64
        else
            BLAS_LIBS       = -lopenblas
        endif
        ifdef SCALAPACK_HOME
        #    SCALAPACK_LIBS  = -L$(SCALAPACK_HOME)/lib -lscalapack -Wl,-rpath=$(SCALAPACK_HOME)/lib64
            SCALAPACK_LIBS  = $(SCALAPACK_HOME)/lib/libscalapack.a
        else
            SCALAPACK_LIBS  = -lscalapack-openmpi
        endif
    endif

#    ifdef NETCDF_HOME
#        NETCDF_INCFLAGS = -I$(NETCDF_HOME)/include
#        NETCDF_LIBS     = -L$(NETCDF_HOME)/lib -lnetcdff -lnetcdf -Wl,-rpath=$(NETCDF_HOME)/lib
#    else
#        NETCDF_LIBS     = -lnetcdff -lnetcdf
#    endif

#    CLIBS           = -lgfortran -lstdc++ -lmpi++ -Wl,--allow-multiple-definition
    CLIBS           =  -lgfortran -lstdc++ -lmpi_cxx -Wl,--allow-multiple-definition
else
    CC              = mpiicc
    CXX             = mpiicpc
    CFLAGS         += 
    INCLUDES        =

    FC              = mpiifort
    FFLAGS         += 
    FINCLUDES       =

    NETCDF_ROOT     = /apps/NETCDF/4.4.1.1/INTEL/IMPI
    NETCDF_INCFLAGS = -I$(NETCDF_ROOT)/include
    NETCDF_LIBS     = -L$(NETCDF_ROOT)/lib -lnetcdff -lnetcdf -Wl,-rpath=$(NETCDF_ROOT)/lib

    ifeq ($(USE_LOCAL_SCALAPACK),FALSE)
        SCALAPACK_LIBS  = -mkl=cluster
    endif
    DEFINES  =
    CLIBS    = -Wl,--allow-multiple-definition -cxxlib
endif

ifeq ($(INSTRUMENT_FUNCTIONS),TRUE)
    FFLAGS   += -finstrument-functions
    CFLAGS   += -finstrument-functions
    CXXFLAGS += -finstrument-functions
endif

LIBS      = $(NETCDF_LIBS) $(BLAS_LIBS) $(LAPACK_LIBS) $(SCALAPACK_LIBS) $(CLIBS) $(EXTRAE_LIB)

FINCLUDES = $(NETCDF_INCFLAGS) -I.

DEFINES  += -DMPI -DFC_HAVE_ABORT -DHAVE_LAPACK -DHAVE_SCALAPACK -DHAVE_MPI -DSIESTA__PEXSI
#  -DCDF  -DMPI_TIMING 

# -DASYNCHRONOUS_GRID_COMMS

ifeq ($(USE_EXTRAE),TRUE)
    ifeq ($(GNU),GNU)
        EXTRAE_HOME  =/apps/BSCTOOLS/extrae/3.7.1/openmpi_1_10_7
    else
        EXTRAE_HOME  =/apps/BSCTOOLS/extrae/3.7.1/impi_2017_4
    endif
    DEFINES     += -D_TRACE_
    LIBS        += -L$(EXTRAE_HOME)/lib -Wl,-rpath=$(EXTRAE_HOME)/lib -lmpitracecf
endif
